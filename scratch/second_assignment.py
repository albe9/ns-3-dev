import json
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
import numpy as np
import os
import pathlib
import math


def process_json(json_dic):

    # Riformatto i dati estratti dal json per essere più accessibili

    sim_data = {}
    for sim in json_dic.values():
        graph_name = sim["simName"]
        wifi_model = sim["wifiModel"]
        if graph_name not in sim_data:
            sim_data[graph_name] = {}

        if wifi_model not in sim_data[graph_name]:
            sim_data[graph_name][wifi_model] = {}
            for key in sim.keys():
                if key != "simName" and key != "wifiModel":
                    sim_data[graph_name][wifi_model][key] = []

        for key in sim.keys():
            if key != "simName" and key != "wifiModel":
                sim_data[graph_name][wifi_model][key].append(sim[key])          
    
    # La dimensione di un qualsiasi array mi dice il numero di simulazioni svolte (Per config è lo stesso tra yans e spectrum)
    # n_simulation = len(sim_data["firstGraph"]["yans"]["apChannels"])

    # Trasformo le coordinate in numpy array
    for graph_key, graph_data in sim_data.items(): 
        # La dimensione di un qualsiasi array mi dice il numero di simulazioni svolte (Per config è lo stesso tra yans e spectrum)
        n_simulation = len(sim_data[graph_key]["yans"]["apChannels"])
        for wifi_model_key, wifi_model_data in graph_data.items():
            sim_data[graph_key][wifi_model_key]["apVectors"] = []
            sim_data[graph_key][wifi_model_key]["staVectors"] = []
            
            for sim_idx in range(n_simulation):
                
                ap_coords = wifi_model_data["apCoords"][sim_idx]
                sta_coords = wifi_model_data["staCoords"][sim_idx]
                
                

                ap_vectors = []
                for coords in ap_coords:
                    splitted = coords.split(":")
                    vector = np.array((float(splitted[0]),float(splitted[1]),float(splitted[2])))
                    ap_vectors.append(vector)
                
                sta_vectors = []
                for coords in sta_coords:
                    splitted = coords.split(":")
                    vector = np.array((float(splitted[0]),float(splitted[1]),float(splitted[2])))
                    sta_vectors.append(vector)

                sim_data[graph_key][wifi_model_key]["apVectors"].append(ap_vectors)
                sim_data[graph_key][wifi_model_key]["staVectors"].append(sta_vectors)   


    # print(sim_data["firstGraph"]["yans"])
    # print(sim_data["firstGraph"]["spectrum"])

    return sim_data

def plot_graph1(sim_data, graph_name, title):

    fig = plt.figure(figsize=[10, 4])
    n_simulation = len(sim_data[graph_name]["yans"]["apChannels"])

    avg_yans_cpu_time = int(sum(sim_data[graph_name]["yans"]["cpuTime4Simulation"]) / n_simulation)
    avg_spectrum_cpu_time = int(sum(sim_data[graph_name]["spectrum"]["cpuTime4Simulation"]) / n_simulation)
    fig.suptitle(f"{title}\navg_yans_cpu_time={avg_yans_cpu_time}ms , avg_spectrum_cpu_time={avg_spectrum_cpu_time}ms")
    axes = []
    axes.append(fig.subplots(1))
    
    throughputs_yans0 = [float(throughput[0]) for throughput in sim_data[graph_name]["yans"]["throughputs"]]
    throughputs_yans1 = [float(throughput[1]) for throughput in sim_data[graph_name]["yans"]["throughputs"]]
    throughputs_spectrum0 = [float(throughput[0]) for throughput in sim_data[graph_name]["spectrum"]["throughputs"]]
    throughputs_spectrum1 = [float(throughput[1]) for throughput in sim_data[graph_name]["spectrum"]["throughputs"]]
    distances = []
    for sim_idx in range(n_simulation):
        distances.append(int(np.linalg.norm(sim_data[graph_name]["yans"]["apVectors"][sim_idx] - sim_data[graph_name]["yans"]["staVectors"][sim_idx][0])))
    
    dark_red_rgba = (0.8, 0, 0, 1)
    light_red_rgba = (0.6, 0, 0, 0.8)
    dark_blue_rgba = (0.4, 0.4, 0.6, 1)
    light_blue_rgba = (0.2, 0.4, 0.6, 0.8)
    axes[0].plot(distances, throughputs_yans0, marker= 'H', color = dark_red_rgba , label = "ap0 -> sta0 (Yans)")
    axes[0].plot(distances, throughputs_yans1, marker= 'H', color = light_red_rgba, label = "ap0 -> sta1 (Yans)")
    axes[0].plot(distances, throughputs_spectrum0, marker= 'H', color = dark_blue_rgba, label = "ap0 -> sta0 (Spectrum)")
    axes[0].plot(distances, throughputs_spectrum1, marker= 'H', color = light_blue_rgba, label = "ap0 -> sta1 (Spectrum)")
    axes[0].set_ylabel("Throughput (Mbps)")
    axes[0].set_xlabel("Ap - Sta Distance in meters")
    
    plt.legend()

    plt.show()

def plot_graph2(sim_data, graph_name, title):
    n_simulation = len(sim_data[graph_name]["yans"]["apChannels"])
    fig = plt.figure(figsize=[10, 4])


    avg_yans_cpu_time = int(sum(sim_data[graph_name]["yans"]["cpuTime4Simulation"]) / n_simulation)
    avg_spectrum_cpu_time = int(sum(sim_data[graph_name]["spectrum"]["cpuTime4Simulation"]) / n_simulation)
    fig.suptitle(f"{title}\navg_yans_cpu_time={avg_yans_cpu_time}ms , avg_spectrum_cpu_time={avg_spectrum_cpu_time}ms")

    axes = []
    axes.append(fig.subplots(1))
    
    
    throughputs_yans0 = [float(throughput[0]) for throughput in sim_data[graph_name]["yans"]["throughputs"]]
    throughputs_yans1 = [float(throughput[1]) for throughput in sim_data[graph_name]["yans"]["throughputs"]]
    throughputs_spectrum0 = [float(throughput[0]) for throughput in sim_data[graph_name]["spectrum"]["throughputs"]]
    throughputs_spectrum1 = [float(throughput[1]) for throughput in sim_data[graph_name]["spectrum"]["throughputs"]]
    distances = []
    for sim_idx in range(n_simulation):
        distances.append(int(np.linalg.norm(sim_data[graph_name]["yans"]["apVectors"][sim_idx][0] - sim_data[graph_name]["yans"]["apVectors"][sim_idx][1])))
    
    
    dark_red_rgba = (0.8, 0, 0, 1)
    light_red_rgba = (0.6, 0, 0, 0.8)
    dark_blue_rgba = (0.4, 0.4, 0.6, 1)
    light_blue_rgba = (0.2, 0.4, 0.6, 0.8)
    axes[0].plot(distances, throughputs_yans0, marker= 'H', color = dark_red_rgba , label = "ap0 -> sta0 (Yans)")
    axes[0].plot(distances, throughputs_yans1, marker= 'H', color = light_red_rgba, label = "ap1 -> sta1 (Yans)")
    axes[0].plot(distances, throughputs_spectrum0, marker= 'H', color = dark_blue_rgba, label = "ap0 -> sta0 (Spectrum)")
    axes[0].plot(distances, throughputs_spectrum1, marker= 'H', color = light_blue_rgba, label = "ap1 -> sta1 (Spectrum)")
    axes[0].set_ylabel("Throughput (Mbps)")
    axes[0].set_xlabel("Ap - Ap Distance in meters")


    plt.legend()

    plt.show()


def plot_interference(sim_data, graph_name, title):
    n_simulation = len(sim_data[graph_name]["yans"]["apChannels"])
    fig = plt.figure(figsize=[10, 4])

    fig.suptitle(title)

    axes = []
    axes.append(fig.subplots(1))
    
    
    yans_RxFailure = sim_data[graph_name]["yans"]["nInterferenceDropPhyRx"]
    spectrum_RxFailure = sim_data[graph_name]["spectrum"]["nInterferenceDropPhyRx"]
    distances = []
    for sim_idx in range(n_simulation):
        distances.append(int(np.linalg.norm(sim_data[graph_name]["yans"]["apVectors"][sim_idx][0] - sim_data[graph_name]["yans"]["apVectors"][sim_idx][1])))
    
    
    dark_red_rgba = (0.8, 0, 0, 1)
    dark_blue_rgba = (0.4, 0.4, 0.6, 1)
    axes[0].plot(distances, yans_RxFailure, marker= 'H', color = dark_red_rgba , label = "yans PhyRx drop")

    axes[0].plot(distances, spectrum_RxFailure, marker= 'H', color = dark_blue_rgba, label = "spectrum PhyRx drop")

    axes[0].set_ylabel("Packet dropped")
    axes[0].set_xlabel("Ap - Ap Distance in meters")


    plt.legend()

    plt.show()


FILE_DIR = pathlib.Path(__file__).parent.resolve()
NS3_DIR =  pathlib.Path(FILE_DIR).parent.resolve()
os.chdir(NS3_DIR)

def main():

    # I json sono stati precedentemente generati e rinominati
    # Il setup delle configurazioni è eseguito e brevevemente descritto nella funzione SetupSimConfigurations() in second_assignment.cc

    # Primo grafico
    # path_to_json1 = "scratch/sim1.json"
    # json_dic1 = json.load(open(path_to_json1, "r"))
    # sim_data1 = process_json(json_dic1)
    # plot_graph1(sim_data1, "firstGraph", "1 Ap --> 2 Sta , channel 1")

    # Secondo grafico
    # path_to_json2 = "scratch/sim2.json"
    # json_dic2 = json.load(open(path_to_json2, "r"))
    # sim_data2 = process_json(json_dic2)
    # plot_graph2(sim_data2, "secondGraph", "2 Ap --> 2 Sta , non overlapping channels (1, 6)")

    # Terzo grafico
    # path_to_json3 = "scratch/sim3.json"
    # json_dic3 = json.load(open(path_to_json3, "r"))
    # sim_data3 = process_json(json_dic3)
    # plot_graph2(sim_data3, "thirdGraph", "2 Ap --> 2 Sta , overlapping channels (1, 2)")

    # Quarto grafico
    path_to_json3 = "scratch/sim3.json"
    json_dic3 = json.load(open(path_to_json3, "r"))
    sim_data3 = process_json(json_dic3)
    plot_interference(sim_data3, "thirdGraph", "Interference comparison (previus graph)")


if __name__ == "__main__":
    main()