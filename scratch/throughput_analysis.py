import subprocess
import json
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
import numpy as np
import pathlib
import os

def make_simulations(path_to_json, n_nodes, channel_datarates, sim_names, flows):

    with open(path_to_json, mode='w+') as f :
        f.write("{\n")

    for sim_idx, (name, flow, channel_datarate) in enumerate(zip(sim_names, flows, channel_datarates)):

        print(f"Simulazione {sim_idx + 1}/{len(sim_names)}")

        command = f"\"scratch/throughput_analysis.cc --nNode={n_nodes} --channelDataRate={channel_datarate} --simulationName={name} --pathToJson={path_to_json} --senderReciverPairs={flow}\""

        simulation = subprocess.Popen(["./ns3", "run", command], stdout=subprocess.DEVNULL)
        simulation.communicate()

        with open(path_to_json, mode='a') as f :
            if name != sim_names[len(sim_names) - 1]:
                f.write(",\n")
            else:
                f.write("\n}")

def plot_throughput(json, channel_datarates):

    n_simulation = len(json.keys())

    fig = plt.figure(figsize=[10, 4 * n_simulation])
    fig.suptitle("Throughput analysis [Csma, 10 Nodes connected, TCP protocol, 5s simulation time]")
    if n_simulation > 1 :
        axes = fig.subplots(n_simulation)
        plt.subplots_adjust(hspace = 0.4)
    else:
        axes = []
        axes.append(fig.subplots(n_simulation))

    for sim_idx in range(n_simulation):

        throughputs = [flow["Throughput"] for flow in json[str(sim_idx)]]
        packet_loss = [float(flow["Packet_loss"]) for flow in json[str(sim_idx)]]
        labels = [(flow["Source_ip"], flow["Destination_ip"]) for flow in json[str(sim_idx)]]
        for label_idx, label in enumerate(labels):
            labels[label_idx] = f"{label[0].rsplit('.', 1)[1]} --> {label[1].rsplit('.', 1)[1]}"
        width = 0.25  
       
        axes[sim_idx].set_xticks(np.arange(len(json[str(sim_idx)])) + width, labels)
        axes[sim_idx].bar(np.arange(len(json[str(sim_idx)])) + width, throughputs, width= width)
        axes[sim_idx].set_ylim(0, int(channel_datarates[sim_idx].removesuffix("Mbps")))
        axes[sim_idx].set_ylabel("Throughput (Mbps)")
        axes[sim_idx].set_xlabel("Node to node flows")

        mean_packet_loss = "{:.1e}".format(sum(packet_loss))

        legend_handles = [ Line2D([0], [0], lw=4, color='r', label=f"Total throughput : {round(sum(throughputs),2)} Mbps", markersize=15),
                          Line2D([0], [0], lw=4, color='r', label=f"Mean Packet loss : {mean_packet_loss}%", markersize=15),
                          Line2D([0], [0], lw=4, color='r', label=f"Channel delay : 1 ms", markersize=15),
                            Line2D([0], [0], lw=4, color='r', label=f"Channel datarate : {channel_datarates[sim_idx]}", markersize=15)]
        axes[sim_idx].legend(handles = legend_handles)

    plt.show()

FILE_DIR = pathlib.Path(__file__).parent.resolve()
NS3_DIR =  pathlib.Path(FILE_DIR).parent.resolve()
os.chdir(NS3_DIR)

def main():
    path_to_json = "scratch/throughput/simulation_data.json"
    n_nodes = 10

    # Esempio base
    flow_pairs = ["0,1;2,3;"]
    channel_datarates = ["100Mbps"]

    # Conf 1
    # flow_pairs = [ "0,1;",
    #                "0,1;0,2;",
    #                "0,1;0,2;0,3;",
    #                "0,1;0,2;0,3;0,4;",
    #                "0,1;0,2;0,3;0,4;0,5;"
    #              ]
    # channel_datarates = ["1000Mbps", "1000Mbps", "1000Mbps", "1000Mbps", "1000Mbps"]

    # Conf 2
    # flow_pairs = [ "0,9;",
    #                "0,9;1,8;",
    #                "0,9;1,8;2,7;",
    #                "0,9;1,8;2,7;3,6;",
    #                "0,9;1,8;2,7;3,6;4,5;"
    #              ]
    # channel_datarates = ["1000Mbps", "1000Mbps", "1000Mbps", "1000Mbps", "1000Mbps"]

    # Conf 3
    # flow_pairs = [ 
    #                "0,1;2,3;4,5;6,7;8,9;",
    #                "0,1;2,3;4,5;6,7;8,9;",
    #                "0,1;2,3;4,5;6,7;8,9;",
    #                "0,1;2,3;4,5;6,7;8,9;",
    #                "0,1;2,3;4,5;6,7;8,9;"
    #              ]
    # channel_datarates = ["10Mbps", "50Mbps", "100Mbps", "500Mbps", "1000Mbps"]

    
    # Esegue il run di ns3 sul file throughput_analysis.cc passando gli argomenti a linea di comando
    # Una volta eseguito viene compilato un file json con tutti i dati delle simulazioni
    make_simulations(path_to_json, n_nodes, channel_datarates, range(len(flow_pairs)), flow_pairs)

    # Genero un dict tramite il file json e ne effetto il plotting
    througput_json = json.load(open(path_to_json, "r"))
    plot_throughput(througput_json, channel_datarates)
    

if __name__ == "__main__":
    main()