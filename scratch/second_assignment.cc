/*
Costruire una rete formata da almeno 4 nodi wireless: 2 Access Points  (AP) e 2 nodi (STA). Se necessario, aumentare il numero di nodi STA.

I due AP devono essere "vicini" (spazialmente) in modo da subire mutua interferenza. Impostare i 2 AP in modo che usino 2 canali WiFi tali da generare mutua interferenza 
(e.g., canali 1 e 2 per 802.11b). Ci si riferisca, per esempio a https://en.wikipedia.org/wiki/List_of_WLAN_channels.

Si associno i nodi STA a uno dei due AP, in modo che parte siano associati a un AP, e parte all'altro.
[opzionale] Si connettano i due AP tramite una rete CSMA o P2P. In questo caso si provveda a mettere le necessarie regole di routing.
Si generi traffico da o per i nodi STA (a piacere).
La simulazione deve essere in grado di usare, alternativamente, i seguenti modelli per il canale:

    YansWiFiChannel
    SpectrumWiFiChannel (sia SpectrumChannel che MultiModelSpectrumChannel)
SI verifichi che:

1)  I risultati della simulazione siano coerenti tra i vari modelli di canale se si usa un solo AP.
2)  I risultati della simulazione siano differenti tra i modelli Yans e Spectrum se si usano due AP.
3)  I risultati della simulazione siano coerenti tra i modelli SpectrumChannel e MultiModelSpectrumChannel se si usano due AP.
4)  La velocità di esecuzione della simulazione nei vari casi

Bonus point per la creatività nel misurare il tempo di esecuzione della simulazione. Vince chi usa il metodo più preciso.
*/


#include <iostream>
#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/flow-monitor-module.h"

#include "ns3/network-module.h"

#include "ns3/yans-wifi-helper.h"
#include "ns3/spectrum-wifi-helper.h"
#include "ns3/spectrum-module.h"
#include "ns3/ssid.h"
#include "ns3/wifi-module.h"

#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/mobility-helper.h"

#include "ns3/internet-module.h"
#include "ns3/ipv4-global-routing-helper.h"

#include <vector>

using namespace ns3;


void setYansPhyChannel(YansWifiPhyHelper* yansPhy, std::string errorModelType)
{
    YansWifiChannelHelper yansChannel;
    yansChannel.AddPropagationLoss("ns3::FriisPropagationLossModel",
                                "Frequency",
                                DoubleValue(2.412e9));
    yansChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
    yansPhy->SetChannel(yansChannel.Create());
    yansPhy->SetErrorRateModel(errorModelType);
    yansPhy->Set("TxPowerStart", DoubleValue(1)); // dBm (1.26 mW)
    yansPhy->Set("TxPowerEnd", DoubleValue(1));
}
void setSpectrumPhyChannel(SpectrumWifiPhyHelper* spectrumPhy, std::string errorModelType){
    Ptr<MultiModelSpectrumChannel> spectrumChannel =
        CreateObject<MultiModelSpectrumChannel>();
    Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
    lossModel->SetFrequency(2.412e9);
    spectrumChannel->AddPropagationLossModel(lossModel);

    Ptr<ConstantSpeedPropagationDelayModel> delayModel =
                CreateObject<ConstantSpeedPropagationDelayModel>();
            spectrumChannel->SetPropagationDelayModel(delayModel);

    spectrumPhy->SetChannel(spectrumChannel);
    spectrumPhy->SetErrorRateModel(errorModelType);
    spectrumPhy->Set("TxPowerStart", DoubleValue(1)); // dBm  (1.26 mW)
    spectrumPhy->Set("TxPowerEnd", DoubleValue(1));
}

template<typename selectedWifiPhy>
void addNetDevice(selectedWifiPhy* genericWifiPhy, std::string macName, StringValue channelSettings,
                    WifiHelper  *wifi, WifiMacHelper  *mac, Ssid  *ssid,
                    Ptr<Node> node, NetDeviceContainer *netDevice)
{
    mac->SetType(macName, "Ssid", SsidValue(*ssid));
    genericWifiPhy->Set("ChannelSettings", channelSettings);
    netDevice->Add(wifi->Install(*genericWifiPhy, *mac, node));
}

Ptr<Application> generateSink(Ptr<Node> installNode, uint32_t simulationTime)
{
    uint16_t port = 9;
    Address localAddress(InetSocketAddress(Ipv4Address::GetAny(), port));
    PacketSinkHelper packetSinkHelper("ns3::TcpSocketFactory", localAddress);
    ApplicationContainer sinkAppContainer;
    sinkAppContainer = packetSinkHelper.Install(installNode);
    sinkAppContainer.Start(Seconds(0.0));
    sinkAppContainer.Stop(Seconds(simulationTime + 1));

    return sinkAppContainer.Get(0);
}

Ptr<Application> generateOnOff(Ptr<Node> installNode, Ipv4Address remoteNodeAddress, uint32_t tcpPayloadSize, uint32_t simulationTime)
{
    uint16_t port = 9;
    OnOffHelper onoff("ns3::TcpSocketFactory", Ipv4Address::GetAny());
    onoff.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
    onoff.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
    onoff.SetAttribute("PacketSize", UintegerValue(tcpPayloadSize));
    onoff.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
    AddressValue remoteAddress(InetSocketAddress(remoteNodeAddress, port));
    onoff.SetAttribute("Remote", remoteAddress);
    ApplicationContainer onOffAppContainer = onoff.Install(installNode);
    onOffAppContainer.Start(Seconds(1.0));
    onOffAppContainer.Stop(Seconds(simulationTime + 1));

    return onOffAppContainer.Get(0);
}

class SimulationStats{
    private:
        // Config della simulazione
        std::string simName = "";
        uint32_t nAp = 0;
        uint32_t nSta = 0;
        std::string wifiModel = "";  
        std::vector<std::string> apChannels;
        std::vector<std::string> staChannels;
        std::vector<Vector> apCoords; 
        std::vector<Vector> staCoords;
        std::vector<std::pair<uint32_t, u_int32_t>> flows;
        // Dati estratti
        int64_t cpuTime4Simulation = 0;
        std::vector<double> throughputs;
        // Dati per le Callback
        uint32_t nGenericErrorPhyRx = 0;
        uint32_t nInterferenceDropPhyRx = 0;
        
        

    public:
        SimulationStats(std::string _simName,
                        uint32_t _nAp,
                        uint32_t _nSta,
                        std::string _wifiModel,  
                        std::vector<std::string> _apChannels,
                        std::vector<std::string> _staChannels,
                        std::vector<Vector> _apCoords, 
                        std::vector<Vector> _staCoords,
                        std::vector<std::pair<uint32_t, u_int32_t>> _flows
                        )
        {
            simName = _simName;
            nAp = _nAp;
            nSta = _nSta;
            wifiModel = _wifiModel;
            apChannels = _apChannels;
            staChannels = _staChannels;
            apCoords = _apCoords;
            staCoords = _staCoords;
            flows = _flows;
        }

        void addStats(int64_t _cpuTime4Simulation, std::vector<double> _throughputs)
        {
            cpuTime4Simulation = _cpuTime4Simulation;
            throughputs = _throughputs;
        }

        uint32_t get_nAp(){return nAp;}
        uint32_t get_nSta(){return nSta;}
        std::string get_wifiModel(){return wifiModel;}  
        std::vector<std::string> get_apChannels(){return apChannels;}
        std::vector<std::string> get_staChannels(){return staChannels;}
        std::vector<Vector> get_apCoords(){return apCoords;} 
        std::vector<Vector> get_staCoords(){return staCoords;}
        std::vector<std::pair<uint32_t, u_int32_t>> get_flows(){return flows;}

        void CallbackWifiPhyRxFailure1(Ptr< const Packet > packet, double snr)
        {
            nGenericErrorPhyRx++;
        }
        void CallbackWifiPhyRxFailure2(ns3::Ptr<ns3::Packet const> packet, WifiPhyRxfailureReason reason)
        {
            // Uno dei motivi per questa failure (nel nostro caso quasi certamente) è l'interferenza tra canali
            if(reason == PREAMBLE_DETECT_FAILURE)
            {
                nInterferenceDropPhyRx++;
            }
        }

        void SerializeToJson(std::ostream *toJson, uint32_t indentLevel = 0)
        {
            std::string indentPrefix = "";
            for(uint32_t indentIdx = 0; indentIdx < indentLevel; indentIdx ++)
            {
                indentPrefix += "\t";
            }
             
            *toJson << indentPrefix << "{" << std::endl;
            *toJson << indentPrefix << "\t" << "\"simName\": \"" << simName << "\"," << std::endl;
            *toJson << indentPrefix << "\t" << "\"wifiModel\": \"" << wifiModel << "\"," << std::endl;
            *toJson << indentPrefix << "\t" << "\"apChannels\": " << SerializeArray(apChannels) << "," << std::endl;
            *toJson << indentPrefix << "\t" << "\"staChannels\": " << SerializeArray(staChannels) << "," << std::endl;
            *toJson << indentPrefix << "\t" << "\"apCoords\": " << SerializeArray(apCoords) << "," << std::endl;
            *toJson << indentPrefix << "\t" << "\"staCoords\": " << SerializeArray(staCoords) << "," << std::endl;
            *toJson << indentPrefix << "\t" << "\"flows\": " << SerializeArray(flows) << "," <<std::endl;
            *toJson << indentPrefix << "\t" << "\"cpuTime4Simulation\": " << cpuTime4Simulation << "," << std::endl;
            *toJson << indentPrefix << "\t" << "\"throughputs\": " << SerializeArray(throughputs) << "," << std::endl;
            *toJson << indentPrefix << "\t" << "\"nGenericErrorPhyRx\": " << nGenericErrorPhyRx << "," << std::endl;
            *toJson << indentPrefix << "\t" << "\"nInterferenceDropPhyRx\": " << nInterferenceDropPhyRx << std::endl;
            *toJson << indentPrefix << "}";
        }

    private:
        template <typename array>
        std::string SerializeArray(array arrayToSerialize)
        {
            std::ostringstream serializedArray ;
            serializedArray << "[";
            for(typename array::iterator iter=arrayToSerialize.begin(); iter != arrayToSerialize.end(); iter++)
            {
                if(iter != arrayToSerialize.begin()) serializedArray << ", ";
                serializedArray << "\"" <<*iter << "\"";
            }
            serializedArray << "]";

            return serializedArray.str();
        }
        

};

class SimulationStatsContainer{
    private:
        std::vector<SimulationStats> simStatsArray;
    public:
        SimulationStatsContainer(){}

        void AddSimStats(SimulationStats simStats)
        {
            simStatsArray.push_back(simStats);
        }

        void SerializeToJson(std::ostream *toJson)
        {
            uint32_t sim_idx = 0;
            *toJson << "{" << std::endl;
            for(std::vector<SimulationStats>::iterator sim_iter=simStatsArray.begin(); sim_iter!=simStatsArray.end(); sim_iter++)
            {
                if(sim_iter != simStatsArray.begin())
                {
                    *toJson << "," << std::endl;
                }
                *toJson << "\t\"" << sim_idx << "\":" << std::endl;
                sim_iter->SerializeToJson(toJson, 1);
                sim_idx++;
                
            }
            *toJson << std::endl << "}" << std::endl;
        }
};

void SetupSimConfigurations(std::vector<SimulationStats> *simStatsList)
{
    //Per motivi di praticità (e velocità) sono state commentate le simulazioni per eseguirle singolarmente.
    //Resta comunque possibile eseguirle tutte ed ottenere un unico json ben formattato.

    // Prima topologia
    /*
                 Ap0
              /     \
            /         \
          Sta0 <--->  Sta1
              distanza 
              variabile

    */
    // std::string simName = "firstGraph";
    // uint32_t nAp = 1;
    // uint32_t nSta = 2;
    // std::string wifiModel = "yans";  
    // std::vector<std::string> apChannels = {"1"};
    // std::vector<std::string> staChannels = {"1", "1"}; 
    // std::vector<Vector> apCoords = {Vector(0.0, 60.0, 0.0)};
    // std::vector<Vector> staCoords = {Vector(-5.0, 0.0, 0.0),
    //                                  Vector(5.0, 0.0, 0.0)};
    // std::vector<std::pair<uint32_t, u_int32_t>> flows = {std::pair(0,0), 
    //                                                      std::pair(0,1)};   

    // for(u_int32_t idx=0; idx<4; idx++){
    //     simStatsList->push_back( SimulationStats(simName, nAp, nSta, wifiModel, apChannels, staChannels, apCoords, staCoords, flows));
    //     staCoords[0].x -= 20; 
    //     staCoords[1].x += 20;
    // }
    // wifiModel = "spectrum";
    // staCoords[0].x = -5;
    // staCoords[1].x = 5;
    // for(u_int32_t idx=0; idx<4; idx++){
    //     simStatsList->push_back( SimulationStats(simName, nAp, nSta, wifiModel, apChannels, staChannels, apCoords, staCoords, flows));
    //     staCoords[0].x -= 20; 
    //     staCoords[1].x += 20;
    // }

    // Seconda e terza topologia (nella seconda canali (1,6), nella terza (1,2))
    /*
              distanza 
              variabile
             Ap0 <--> Ap1
              /        \
            /           \
          Sta0          Sta1
              

    */
    std::string simName = "secondGraph";
    uint32_t nAp = 2;
    uint32_t nSta = 2;
    std::string wifiModel = "yans";  
    std::vector<std::string> apChannels = {"1",  "6"};
    std::vector<std::string> staChannels = {"1", "6"}; 
    std::vector<Vector> apCoords = {Vector(-10.0, 50.0, 0.0),
                                    Vector(10.0, 50.0, 0.0)};
    std::vector<Vector> staCoords = {Vector(-10.0, 0.0, 0.0),
                                     Vector(10.0, 0.0, 0.0)};
    std::vector<std::pair<uint32_t, u_int32_t>> flows = {std::pair(0,0), 
                                                         std::pair(1,1)};   

    for(u_int32_t idx=0; idx<5; idx++){
        simStatsList->push_back( SimulationStats(simName, nAp, nSta, wifiModel, apChannels, staChannels, apCoords, staCoords, flows));
        apCoords[0].x -= 25; 
        apCoords[1].x += 25;
    }
    wifiModel = "spectrum";
    apCoords[0].x = -10;
    apCoords[1].x = 10;
    for(u_int32_t idx=0; idx<5; idx++){
        simStatsList->push_back( SimulationStats(simName, nAp, nSta, wifiModel, apChannels, staChannels, apCoords, staCoords, flows));
        apCoords[0].x -= 25; 
        apCoords[1].x += 25;
    }

    // std::string simName = "thirdGraph";
    // apChannels = {"1",  "2"};
    // staChannels = {"1",  "2"};

    // for(u_int32_t idx=0; idx<5; idx++){
    //     simStatsList->push_back( SimulationStats(simName, nAp, nSta, wifiModel, apChannels, staChannels, apCoords, staCoords, flows));
    //     apCoords[0].x -= 25; 
    //     apCoords[1].x += 25;
    // }
    // wifiModel = "spectrum";
    // apCoords[0].x = -10;
    // apCoords[1].x = 10;
    // for(u_int32_t idx=0; idx<5; idx++){
    //     simStatsList->push_back( SimulationStats(simName, nAp, nSta, wifiModel, apChannels, staChannels, apCoords, staCoords, flows));
    //     apCoords[0].x -= 25; 
    //     apCoords[1].x += 25;
    // }



}



int
main(int argc, char* argv[])
{   
    //Variabili base per le simulazioni
    uint32_t nAp = 0;
    uint32_t nSta = 0;
    std::string errorModelType = "ns3::NistErrorRateModel";
    std::string wifiModel = "";   //yans o spectrum
    uint32_t simulationTime = 5; 
    const uint32_t tcpPayloadSize = 1024;
    Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(tcpPayloadSize));
    
    std::vector<std::string> apChannels;    // canali degli ap (assegnati in ordine)
    std::vector<std::string> staChannels;   // canali degli sta (assegnati in ordine)

    std::vector<Vector> apCoords;   // coordinate degli ap (assegnate in ordine)
    std::vector<Vector> staCoords;  // coordinate delle sta (assegnate in ordine)

    std::vector<std::pair<uint32_t, u_int32_t>> flows; 
    
    SeedManager::SetSeed(10);

    // Contenitore per i dati delle simulazioni
    SimulationStatsContainer simStatsContainer;

    //Config e setup per le simulazioni 
    std::vector<SimulationStats> simStatsList;
    SetupSimConfigurations(&simStatsList);

    uint32_t total_sim = simStatsList.size();
    uint32_t current_sim = 1;
    //Eesecuzione  e raccolta dati dalle simulazioni
    for(std::vector<SimulationStats>::iterator sim_iter=simStatsList.begin(); sim_iter!=simStatsList.end(); sim_iter++)
    {
        std::cout << "Simulazione " << current_sim << "/" << total_sim << std::endl;
        current_sim++;

        nAp = sim_iter->get_nAp();
        nSta = sim_iter->get_nSta();
        
        wifiModel = sim_iter->get_wifiModel();   
        
        apChannels = sim_iter->get_apChannels();
        staChannels = sim_iter->get_staChannels();

        apCoords = sim_iter->get_apCoords(); 
        staCoords = sim_iter->get_staCoords();

        flows = sim_iter->get_flows();

        NodeContainer wifiStaNode;
        wifiStaNode.Create(nSta);
        NodeContainer wifiApNode;
        wifiApNode.Create(nAp);

        YansWifiPhyHelper yansPhy;
        SpectrumWifiPhyHelper spectrumPhy1;

        // Setup del canale, del lossmodel e del delay
        // ho utilizato friis come losspropagationmodel settando la stessa frequenza (canale 1) a prescindere dal canale utilizzato
        // Controllando la furmula, poichè le frequenze tra differiscono di pochi Mhz non dovrebbe influire molto
        if (wifiModel == "yans")
        {
            setYansPhyChannel(&yansPhy, errorModelType);

        }
        else if (wifiModel == "spectrum")
        {
            setSpectrumPhyChannel(&spectrumPhy1, errorModelType);
        }
        


        WifiHelper wifi;
        wifi.SetStandard(WIFI_STANDARD_80211n);
        //  wifi.SetStandard(WIFI_STANDARD_80211g);

        StringValue DataRate("HtMcs4");
        // StringValue DataRate("DsssRate11Mbps");
        wifi.SetRemoteStationManager("ns3::ConstantRateWifiManager",
                                        "DataMode",
                                        DataRate,
                                        "ControlMode",
                                        DataRate);
        WifiMacHelper mac;

        Ssid ssid = Ssid("Second_assignment");


        NetDeviceContainer staDevice;
        NetDeviceContainer apDevice;


        if (wifiModel == "yans")
        {
            // Setup dei NetDevice degli access points
            for(u_int32_t ap_idx=0; ap_idx < nAp; ap_idx++)
            {
                addNetDevice<YansWifiPhyHelper>(&yansPhy, "ns3::ApWifiMac", StringValue(std::string("{") + apChannels[ap_idx] + std::string(", 20, BAND_2_4GHZ, 0}")),
                        &wifi, &mac, &ssid, wifiApNode.Get(ap_idx), &apDevice);
            }
            // Setup dei NetDevice delle stations
            for(u_int32_t sta_idx=0; sta_idx < nSta; sta_idx++)
            {
                addNetDevice<YansWifiPhyHelper>(&yansPhy, "ns3::StaWifiMac", StringValue(std::string("{") + staChannels[sta_idx] + std::string(", 20, BAND_2_4GHZ, 0}")),
                        &wifi, &mac, &ssid, wifiStaNode.Get(sta_idx), &staDevice);
            }
        }
        else if (wifiModel == "spectrum")
        {
            // Setup dei NetDevice degli access points
            for(u_int32_t ap_idx=0; ap_idx < nAp; ap_idx++)
            {
                addNetDevice<SpectrumWifiPhyHelper>(&spectrumPhy1, "ns3::ApWifiMac", StringValue(std::string("{") + apChannels[ap_idx] + std::string(", 20, BAND_2_4GHZ, 0}")),
                        &wifi, &mac, &ssid, wifiApNode.Get(ap_idx), &apDevice);
            }
            // Setup dei NetDevice delle stations
            for(u_int32_t sta_idx=0; sta_idx < nSta; sta_idx++)
            {
                addNetDevice<SpectrumWifiPhyHelper>(&spectrumPhy1, "ns3::StaWifiMac", StringValue(std::string("{") + staChannels[sta_idx] + std::string(", 20, BAND_2_4GHZ, 0}")),
                        &wifi, &mac, &ssid, wifiStaNode.Get(sta_idx), &staDevice);
            }
            
        }

        MobilityHelper mobility;
        Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();

        // Settaggio delle coordinate degli ap e sta
        for(u_int32_t ap_idx=0; ap_idx < nAp; ap_idx++)
        {
            positionAlloc->Add(apCoords[ap_idx]);
        }
        for(u_int32_t sta_idx=0; sta_idx < nSta; sta_idx++)
        {
            positionAlloc->Add(staCoords[sta_idx]);
        }


        mobility.SetPositionAllocator(positionAlloc);

        mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");

        // Assegnazione delle coordinate agli ap e sta
        for(u_int32_t ap_idx=0; ap_idx < nAp; ap_idx++)
        {
            mobility.Install(wifiApNode.Get(ap_idx));
        }
        for(u_int32_t sta_idx=0; sta_idx < nSta; sta_idx++)
        {
            mobility.Install(wifiStaNode.Get(sta_idx));
        }

        // Assegnazione indirizzi (tutti i nodi sulla stessa sottorete)
        InternetStackHelper stack;
        stack.Install(wifiApNode);
        stack.Install(wifiStaNode);

        Ipv4AddressHelper address;
        address.SetBase("192.168.1.0", "255.255.255.0");
        Ipv4InterfaceContainer staNodeInterface;
        Ipv4InterfaceContainer apNodeInterface;

        apNodeInterface = address.Assign(apDevice);
        staNodeInterface = address.Assign(staDevice);
        

        std::vector<Ptr<Application>> sinkAppVector;
        std::vector<Ptr<Application>> onOffAppVector;
        

        // Setup applicazioni in base ai flow definiti es flow = (0,1) ap0 -->sta1
        for(std::vector<std::pair<uint32_t, u_int32_t>>::iterator flow_iter = flows.begin(); flow_iter != flows.end(); flow_iter ++)
        {
            if(flow_iter->first > nAp - 1 || flow_iter->second > nSta - 1)
            {
                std::cerr << "Vettore dei flows non concorde con nAp o nSta" << std::endl;
                exit(-1);
            }
            onOffAppVector.push_back(generateOnOff(wifiApNode.Get(flow_iter->first), staNodeInterface.GetAddress(flow_iter->second), tcpPayloadSize, simulationTime));
            sinkAppVector.push_back(generateSink(wifiStaNode.Get(flow_iter->second), simulationTime));

        }

        // connessione delle callback per raccogliere i dati relativi ai pacchetti droppati nel livello fisico
        Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/Phy/State/RxError", MakeCallback(&SimulationStats::CallbackWifiPhyRxFailure1, &(*sim_iter)));
        Config::ConnectWithoutContext("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/PhyRxDrop", MakeCallback(&SimulationStats::CallbackWifiPhyRxFailure2, &(*sim_iter)));

        FlowMonitorHelper flowMonitor;
        flowMonitor.Install(wifiStaNode);
        flowMonitor.Install(wifiApNode);

        auto start_time = std::chrono::high_resolution_clock::now();

        Simulator::Stop(Seconds(simulationTime + 1));
        Simulator::Run();
        Simulator::Destroy();

        // Calcolo del tempo di computazione della simulazione 
        //(sarebbe stato più preciso utilizzare altre callback, ma non ho fatto in tempo )

        auto end_time = std::chrono::high_resolution_clock::now(); 

        auto ms_elapsed =  std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time);

        int64_t cpuTime4Simulation = ms_elapsed.count();
        // std::cout << cpuTime4Simulation << std::endl;

        // flowMonitor.SerializeToXmlFile("./scratch/test.xml", false, false);

        std::vector<uint64_t> totalBytesRx;
        std::vector<double> throughputs;

        for(u_int32_t sta_idx=0; sta_idx < nSta; sta_idx++)
        {
            totalBytesRx.push_back(DynamicCast<PacketSink>(sinkAppVector[sta_idx])->GetTotalRx());
            throughputs.push_back((totalBytesRx[sta_idx]) * 8 / (simulationTime * 1000000.0)); // Mbit/s
            // std::cout << throughputs[sta_idx] << std::endl;
        }

        sim_iter->addStats(cpuTime4Simulation, throughputs);

        simStatsContainer.AddSimStats(*sim_iter);
        

    }

    //Serializzazione in json di tutti i dati delle simulazioni eseguite
    std::ofstream toJson("./scratch/sim.json");
    simStatsContainer.SerializeToJson( &toJson);

    return(0);
}