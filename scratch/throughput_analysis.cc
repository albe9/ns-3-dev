#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/internet-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/network-module.h"
#include <map>
#include <vector>


using namespace ns3;



void GenerateSender(uint32_t installNodeIdx, uint32_t destNodeIdx, Ipv4InterfaceContainer *interfaces, NodeContainer *nodes)
{
    BulkSendHelper bulkSender("ns3::TcpSocketFactory", InetSocketAddress(interfaces->GetAddress(destNodeIdx), 10));
    bulkSender.SetAttribute("SendSize", UintegerValue(1024));
    bulkSender.SetAttribute("MaxBytes", UintegerValue(0));
    ApplicationContainer clientApp = bulkSender.Install(nodes->Get(installNodeIdx));
    clientApp.Start(Seconds(0.0));
    clientApp.Stop(Seconds(5.0));
}

void GenerateReciver(uint32_t installNodeIdx, Ipv4InterfaceContainer *interfaces, NodeContainer *nodes)
{
    PacketSinkHelper packetSink("ns3::TcpSocketFactory", InetSocketAddress(interfaces->GetAddress(installNodeIdx), 10));
    ApplicationContainer serverApp = packetSink.Install(nodes->Get(installNodeIdx));
    serverApp.Start(Seconds(0.0));
    serverApp.Stop(Seconds(5.0));
}

void SerializeToJsonFile(std::string pathToJson, std::string simulationName, Ptr<FlowMonitor> monitor, Ptr<FlowClassifier> flowClassifier)
{
    std::ofstream jsonFile;

    jsonFile.open(pathToJson, std::ios_base::app);

    jsonFile << "\"" << simulationName << "\" : [\n";

    FlowMonitor::FlowStatsContainer stats = monitor->GetFlowStats();
    Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier>(flowClassifier);
    for (FlowMonitor::FlowStatsContainer::iterator it = stats.begin(); it != stats.end(); it++)
    {
        Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow(it->first);

        jsonFile << "\t{ " << "\"Flow_id\": " << it->first << ", ";
        jsonFile << "\t" << "\"Source_ip\": " << "\"" << t.sourceAddress << "\", ";
        jsonFile << "\t" << "\"Destination_ip\": " << "\"" << t.destinationAddress << "\", ";
        jsonFile << "\t" << "\"Protocol_number\": " << "\"" << (u_int16_t)t.protocol << "\", ";
        jsonFile << "\t" << "\"Packet_loss\": " << "\"" << (it->second.lostPackets * 100.0) / it->second.rxBytes << "\", ";
        jsonFile << "\t" << "\"Throughput\": " << it->second.rxBytes * 8.0 / (5.0 * 1000000) << " }";
        if(std::next(it) != stats.end()){
            jsonFile << ",\n";
        }
        
        // Debug
        // std::cout << "Flow ID: " << it->first << " Source IP: " << t.sourceAddress << " Destination IP: " << t.destinationAddress << std::endl;
        // std::cout << "Throughput: " << it->second.rxBytes * 8.0 / (5.0 * 1000000) << " Mbps" << std::endl;
    }

    jsonFile << "\n]";

    jsonFile.close();


}

int
main(int argc, char* argv[])
{   
    // Topologia della rete
    //
    //      
    // n0  n1   ...   nn-1  nn
    // |    |         |     |
    // ======================
    //     LAN 10.1.1.0


    std::string simulationName="";
    std::string pathToJson="";
    uint32_t nNode = 10;
    std::string senderReciverPairs="";
    std::string channelDataRate="100Mbps";

    CommandLine cmd(__FILE__);    

    cmd.AddValue("simulationName", "Nome della simulazione, utilizzato per fare le serializzazione (in JSON) del flow", simulationName);
    cmd.AddValue("senderReciverPairs", "Coppie sender reciver, es (nodi A,B,C): A,B;A,C;B,C; ", senderReciverPairs);
    cmd.AddValue("pathToJson", "Path del file XML dove sarà trascritto il flow della simulazione", pathToJson);
    cmd.AddValue("nNode", "Numero di nodi della CSMA", nNode);
    cmd.AddValue("channelDataRate", "Data rate del canale (es 100Mbps)", channelDataRate);

    cmd.Parse(argc, argv);

    // Metodo non molto elegamente ma funzionale per fare il parsing e convertire la stringa in un map di interi

    std::string pair;
    std::stringstream ss(senderReciverPairs);
    std::map<int, std::vector<int>> pairs;

    while(std::getline(ss, pair, ';'))
    {
        pairs[std::stoi(pair.substr(0, pair.find(",")))].push_back(std::stoi(pair.substr(pair.find(",") + 1)));
    }


    // 2 MB di TCP buffer
    Config::SetDefault("ns3::TcpSocket::RcvBufSize", UintegerValue(1 << 21));
    Config::SetDefault("ns3::TcpSocket::SndBufSize", UintegerValue(1 << 21));

    NodeContainer nodes;
    nodes.Create(nNode); 

    CsmaHelper csma;

    csma.SetChannelAttribute("DataRate", StringValue(channelDataRate));
    csma.SetChannelAttribute("Delay", TimeValue(NanoSeconds(1000)));

    NetDeviceContainer devices;
    devices = csma.Install(nodes);

    InternetStackHelper stack;
    stack.Install(nodes);

    Ipv4AddressHelper address;
    address.SetBase("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer interfaces;
    interfaces = address.Assign(devices);

    // installo i bulk sender e setto le destinazioni in base al map generato facendo il parsing di senderReciverPairs ottenuto tramite riga di comando

    for(std::map<int, std::vector<int>>::iterator map_it=pairs.begin(); map_it != pairs.end(); map_it++)
    {
        for(std::vector<int>::iterator vec_it=map_it->second.begin(); vec_it != map_it->second.end(); vec_it++)
        {
            // Debug
            // std::cout << "source" << map_it->first << "destination" << *vec_it << std::endl;
            GenerateSender(map_it->first, *vec_it, &interfaces, &nodes);
        }
    }

    // installo i sink su tutti i nodi

    for(uint16_t nodeIdx=0; nodeIdx<nNode; nodeIdx++)
    {
        GenerateReciver(nodeIdx, &interfaces, &nodes);
    } 

    Ipv4GlobalRoutingHelper::PopulateRoutingTables();

    FlowMonitorHelper flowMonitor;
    Ptr<FlowMonitor> monitor = flowMonitor.Install(nodes);

    Simulator::Stop(Seconds(5.0));
    Simulator::Run();
    Simulator::Destroy();

    // Funzione per eseguire la serializzazione su un file json ( comodo per lavorarci con python)
    SerializeToJsonFile(pathToJson, simulationName, monitor, flowMonitor.GetClassifier());
    // monitor->SerializeToXmlFile("prova.xml", false, false);
    
    return(0);
}